import { Menu } from "antd";
import React from "react";
import { Link } from "react-router-dom";
import { useAuth } from "../utils/auth";

const Header = () => {
  const auth = useAuth();

  return (
    <Menu>
      <Menu.Item>
        <Link to="/" />
        Home
      </Menu.Item>
      {!auth.user && (
        <Menu.Item>
          <Link to="/login">Login</Link>
        </Menu.Item>
      )}

      <Menu.Item>
        <Link to="/profile" />
        Profile
      </Menu.Item>
    </Menu>
  );
};

export default Header;
