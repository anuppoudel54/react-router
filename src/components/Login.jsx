import React from "react";
import { useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import { useAuth } from "../utils/auth";

const Login = () => {
  const auth = useAuth();
  const navigate = useNavigate();
  const [user, setUser] = useState("");
  const location = useLocation();
  const redirectPath = location.state?.path || "/";
  const handleLogin = () => {
    auth.login(user);
    navigate(redirectPath, { replace: true });
  };
  return (
    <>
      <div>
        <label>
          username:{""}
          <input
            type="text"
            onChange={(e) => {
              setUser(e.target.value);
            }}
          ></input>
        </label>
        <button onClick={handleLogin}>Login</button>
      </div>
    </>
  );
};

export default Login;
