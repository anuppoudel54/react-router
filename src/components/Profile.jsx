import React from "react";
import { useNavigate } from "react-router-dom";
import { useAuth } from "../utils/auth";

const Profile = () => {
  const navigate = useNavigate();
  const handleLogin = () => {
    auth.logout();
    navigate("/");
  };
  const auth = useAuth();
  return (
    <div>
      Welcome {auth.user}
      <button onClick={handleLogin}> Logout</button>
    </div>
  );
};

export default Profile;
