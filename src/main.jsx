import React from "react";
import App from "./App";
import { render } from "react-dom";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import "antd/dist/antd.css";
import Home from "./components/Home";
import Profile from "./components/Profile";
import Login from "./components/Login";
import { AuthProvider } from "./utils/auth";
import { RequireAuth } from "./utils/RequireAuth";

const rootEL = document.getElementById("root");
render(
  <React.StrictMode>
    <AuthProvider>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<App />}>
            <Route index element={<Home />} />
            <Route
              path="profile"
              element={
                <RequireAuth>
                  <Profile />
                </RequireAuth>
              }
            />
          </Route>
          <Route path="/login" element={<Login />} />
        </Routes>
      </BrowserRouter>
    </AuthProvider>
  </React.StrictMode>,
  rootEL
);
